check the wiki on the project for updates
## for a basic laravel app clone the repository and do:
```bash
# add .env for docker and .env for laravel
cp .env.example .env
cp ./src/.env.example ./src/.env
docker-compose up -d
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache
# your config will be saved into /var/www/bootstrap/cache/config.php 
docker-compose exec app php artisan migrate
```
visit: http://localhost:8085

## to test aimeos for ecommerce look at the branch aimeos
```bash
#change to that branch with
git checkout aimeos
# if you want to start a project from scratch with full aimeos shop do this:
# docker run --rm -v $(pwd)/src:/app composer create-project aimeos/aimeos myshop
#do common stuff
cp .env.example .env
cp ./src/.env.example ./src/.env
docker-compose up -d
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose exec app npm install
docker-compose exec app php artisan vendor:publish --all
docker-compose exec app php artisan migrate
docker-compose exec app php artisan aimeos:setup --option=setup/default/demo:1
docker-compose exec app php artisan aimeos:account --super
docker-compose exec app php artisan cache:clear

visit: http://localhost:8085
http://localhost:8085/shop
http://localhost:8085/admin

To enter into the app container and run commands as www user:
docker-compose exec app bash
To enter as sudo user:  
docker-compose exec --user root app bash
```

## to start an avored project
```bash
git clone gitlab.com:grodrigo.dev/laravel.git avored
cd avored
rm -rf src
mkdir src
cp .env.example .env
## in .env file put: name=avored
nano .env
## change at the end of docker-compose.yml: the network name and the volume name
## instead of aimeos put avored
nano docker-compose.yml
docker-compose up -d
docker-compose exec app composer create-project --prefer-dist avored/laravel-ecommerce
mv src/laravel-ecommerce/* src/
mv src/laravel-ecommerce/.* src/
rm -rf src/laravel-ecommerce/
nano src/.env
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan avored:install
docker-compose exec app php artisan avored:admin:make
```bash
visit:  
http://localhost:8085  
http://localhost:8085/admin


## to start from beginning
```bash
# In the root we'll have docker configuration files and in the src folder we'll put the laravel project
mkdir aimeos
git clone https://gitlab.com/grodrigo.dev/laravel.git .
# or if you want to start a new project with specific laravel version do:
docker run --rm -v $(pwd)/src:/app composer create-project laravel/laravel laravel "6.*"

docker run --rm -v $(pwd)/src:/app composer install
sudo chown -R $USER:$USER ./src
add .env for docker and .env for laravel
cp .env.example .env
cp ./src/.env.example ./src/.env
```

```bash
# notes if you need to install some package (note that if you remove the container you'll lost this changes)
# you'll need intl extension to get running aimeos
docker-compose exec --user root app bash
apt update -y
apt-get install -y zlib1g-dev libicu-dev g++
apt install libzip-dev
docker-php-ext-configure intl
docker-php-ext-install zip
# if necessary pecl install zip

docker-composer restart app
docker-compose exec app composer update

#this is because the Dockerfile I miss, perhaps in the futere add:
    zlib1g-dev \
    libzip-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install zip
```

```bash
#to install npm inside the container do
docker-compose exec --user root app bash
curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get install nodejs -y

# and from outside:
docker-compose exec app npm install
docker-compose exec app php artisan aimeos:account --super
cd src
mkdir public/files public/preview public/uploads
chmod 777 public/files public/preview public/uploads
```

```bash
# notes to have multiple .env files for laravel:
add to the end of bootstrap/app.php
$env = env('APP_ENV', 'production');
$envFile = ".env-$env";
$app->loadEnvironmentFrom($envFile);
return $app;
```

https://github.com/aimeos/aimeos-laravel

