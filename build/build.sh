#!/bin/bash
time docker build \
         --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
         --build-arg VCS_REF=`git --git-dir=../.git rev-parse --short HEAD` \
         --build-arg VERSION=`cat VERSION` \
         -t grodrigo/laravel ../ | tee build.log

# run https://github.com/lukebond/microscanner-wrapper to scann the image
